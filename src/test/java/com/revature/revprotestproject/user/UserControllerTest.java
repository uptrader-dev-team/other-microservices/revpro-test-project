package com.revature.revprotestproject.user;

import com.revature.revprotestproject.ControllerAdvisor;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test for UserController.java
 * Created by: anandak
 * Date created: 8/29/2023
 */
@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    ControllerAdvisor advisor;

    MockMvc mockMvc;

    User user;

    @InjectMocks
    UserController controller;

    public static final String SLASH = "/";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(advisor)
                .build();

        user = new User();
        user.setUserName("test_user_name");
        user.setUserEmail("test_email");
        user.setUserId(1L);
    }

    @Test
    @SneakyThrows
    void persist() {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/users/new/"
                        + user.getUserName() + SLASH
                        + user.getUserEmail()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void getUserId() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/users/get/" + user.getUserName()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }
}