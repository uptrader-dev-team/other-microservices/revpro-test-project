package com.revature.revprotestproject.message;

import com.revature.revprotestproject.ControllerAdvisor;
import com.revature.revprotestproject.user.User;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test for MessageController.java
 * Created by: anandak
 * Date created: 8/28/2023
 */

@Slf4j
@ExtendWith(MockitoExtension.class)
class MessageControllerTest {

    @Mock
    ControllerAdvisor advisor;

    MockMvc mockMvc;

    Message message;

    User user;

    @InjectMocks
    MessageController controller;

    public static final String SLASH = "/";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(advisor)
                .build();

        message = new Message();
        message.setMessage("test_message");
        message.setMessageId(1L);
        message.setUserId(1L);

        user = new User();
        user.setUserName("test_user_name");
        user.setUserEmail("test_email");
        user.setUserId(1L);
    }

    @Test
    @SneakyThrows
    void persist() {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/messages/new/"
                        + user.getUserName() + SLASH
                        + message.getMessage()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void getMessages() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/messages/get/"
                        + user.getUserName()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void displayAll() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/messages/list/"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void delete() {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/messages/delete/"
                        + user.getUserName()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void editMessage() {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/messages/edit/"
                        + user.getUserName() + SLASH
                        + message.getMessage() + SLASH
                        + message.getMessageId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }
}