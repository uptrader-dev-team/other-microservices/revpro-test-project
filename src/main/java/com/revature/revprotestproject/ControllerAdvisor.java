package com.revature.revprotestproject;

import com.revature.revprotestproject.exception.InvalidMessageException;
import com.revature.revprotestproject.exception.InvalidNameException;
import com.revature.revprotestproject.exception.StandardException;
import com.revature.revprotestproject.exception.UserNotFoundException;
import com.revature.revprotestproject.response.ResponseBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

/**
 * Global handler of all exceptions
 * Created by: anandak
 * Date created: 8/29/2023
 */

@Slf4j
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    private ResponseBuilder errorResponseBuilder;

    @Autowired
    public ControllerAdvisor(ResponseBuilder errorResponseBuilder) {
        this.errorResponseBuilder = errorResponseBuilder;
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleException(ex, request, status);
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleUncaughtException(Exception ex, WebRequest request) {
        return handleException(ex, request, null);
    }

    ResponseEntity<Object> handleException(Exception ex, WebRequest request, HttpStatus overrideStatus) {
        ResponseEntity<?> responseEntity = this.errorResponseBuilder.buildErrorResponse(ex);

        if (request != null && HttpStatus.INTERNAL_SERVER_ERROR.equals(responseEntity.getStatusCode())) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, RequestAttributes.SCOPE_REQUEST);
        }

        return (ResponseEntity<Object>) responseEntity;
    }

    @ExceptionHandler(StandardException.class)
    public ResponseEntity<Object> standardError(Exception exception) {
        return errorResponseBuilder.buildResult(
                exception.getMessage(), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(InvalidMessageException.class)
    public ResponseEntity<Object> messageInvalid(Exception exception) {
        return errorResponseBuilder.buildResult(
                exception.getMessage(), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(InvalidNameException.class)
    public ResponseEntity<Object> nameInvalid(Exception exception) {
        return errorResponseBuilder.buildResult(
                exception.getMessage(), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> userNotFound(Exception exception) {
        return errorResponseBuilder.buildResult(
                exception.getMessage(), HttpStatus.BAD_REQUEST
        );
    }
}
