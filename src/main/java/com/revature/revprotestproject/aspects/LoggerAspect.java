package com.revature.revprotestproject.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Modularizes logging across Spring layers
 * Created by: anandak
 * Date created: 8/29/2023
 */

@Slf4j
@Aspect
@Component
public class LoggerAspect {
    @AfterThrowing(pointcut = "execution(* com.revature..*.*(..))", throwing = "ex")
    private void logErr(JoinPoint joinPoint, Exception ex) {
        log.error("Exception: "
                + ex
                + " has been thrown from "
                + joinPoint.getSignature().getName()
                + " method")
        ;
    }
}