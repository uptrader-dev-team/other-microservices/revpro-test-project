package com.revature.revprotestproject;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class WebSecurityConfiguration {

    /*****************************************************************************************************************
     * Other - Basic/Public
     *****************************************************************************************************************/

    @Configuration
    @Order(30)
    public static class HttpSecurityConfiguration extends WebSecurityConfigurerAdapter {

        protected void configure(HttpSecurity http) throws Exception {
            http
                    .cors()
                    .and()
                    .csrf()
                    .disable()
                    .headers()
                    .frameOptions()
                    .disable()
                    .and()
                    .authorizeRequests()
                    .antMatchers("/", "/home").permitAll()
                    .antMatchers("/csrf").permitAll()
                    .antMatchers("/api/**").permitAll()
                    .antMatchers("/error").permitAll()
                    .antMatchers("/swagger-ui/**", "/swagger-resources/**", "/webjars/**", "/v2/api-docs").permitAll()
                    .antMatchers("/graphql").permitAll()
                    .requestMatchers(EndpointRequest.to("info", "health")).permitAll() // actuator
                    .anyRequest().permitAll()
                    .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .httpBasic()
            ;
        }

        @Bean
        public Docket api() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .useDefaultResponseMessages(false)
                    .enableUrlTemplating(false)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.revature"))
                    .paths(PathSelectors.any())
                    .build()
                    .protocols(Stream.of("https", "http").collect(toSet()));
        }

        private ApiInfo apiInfo() {
            return new ApiInfo(
                    "RevPro test project",
                    "API Resources & Documentation",
                    "2.0",
                    "",
                    new Contact("ANANDAK", "", "ANANDAK@ALUM.UTK.EDU"),
                    "API License",
                    "",
                    Collections.emptyList());
        }

    }

    @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager(
            SecurityProperties properties, ObjectProvider<PasswordEncoder> passwordEncoder) {

        return new UserDetailsServiceAutoConfiguration().inMemoryUserDetailsManager(properties, passwordEncoder);
    }
}
