package com.revature.revprotestproject.exception;

/**
 * Creates custom exceptions related to invalid user
 * Created by: anandak
 * Date created: 8/29/2023
 */

public class UserNotFoundException extends Exception {
    public UserNotFoundException(String message) {
        super(message);
    }
}
