package com.revature.revprotestproject.exception;

/**
 * Creates custom exceptions related to invalid naming
 * Created by: anandak
 * Date created: 8/29/2023
 */

public class InvalidNameException extends Exception {
    public InvalidNameException(String message) {
        super(message);
    }
}
