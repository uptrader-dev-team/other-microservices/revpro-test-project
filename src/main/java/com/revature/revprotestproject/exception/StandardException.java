package com.revature.revprotestproject.exception;

/**
 * Creates standard custom exceptions
 * Created by: anandak
 * Date created: 8/29/2022
 */

public class StandardException extends Exception {
    public StandardException(String message) {
        super(message);
    }
}
