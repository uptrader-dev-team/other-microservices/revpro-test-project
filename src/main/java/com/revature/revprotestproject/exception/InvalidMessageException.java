package com.revature.revprotestproject.exception;

/**
 * Creates custom exceptions related to invalid messaging
 * Created by: anandak
 * Date created: 8/29/2023
 */

public class InvalidMessageException extends Exception {
    public InvalidMessageException(String message) {
        super(message);
    }
}
