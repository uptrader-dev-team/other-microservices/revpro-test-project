package com.revature.revprotestproject;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * This class contains all notifications
 * Created by: anandak
 * Date created: 08/29/2023
 */

@ConfigurationProperties(prefix = "rev.notify")
public class Notifications {
    public String error_occurred;
    public String message_not_found;
    public String message_invalid;
    public String message_id_invalid;
    public String user_not_found;
    public String user_name_invalid;
}
