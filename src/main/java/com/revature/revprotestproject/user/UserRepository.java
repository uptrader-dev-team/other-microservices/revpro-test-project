package com.revature.revprotestproject.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    @NonNull
    User save(@NonNull User user);

    @Query("SELECT u.userId FROM User u WHERE u.userName = (:name)")
    Optional<Long> getId(String name);
}
