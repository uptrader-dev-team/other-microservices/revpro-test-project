package com.revature.revprotestproject.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service layer for exposing user info
 * Created by: anandak
 * Date created: 8/28/2023
 */

@Slf4j
@Service
public class UserService {
    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(@NonNull User user) {
        return userRepository.save(user);
    }

    public Optional<Long> getId(String name) {
        return userRepository.getId(name);
    }
}
