package com.revature.revprotestproject.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * JPA entity for persisting user info
 * Created by: anandak
 * Date created: 8/28/2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    Long userId;

    @Column(name = "user_name")
    String userName;

    @Column(name = "user_email ")
    String userEmail;

    @CreationTimestamp
    @Column(name = "created_date")
    private Timestamp createdDate;

    @Version
    @UpdateTimestamp
    @Column(name = "modified_date")
    private Timestamp lastModifiedDate;

    @PrePersist
    void createdDate() {
        this.createdDate = new Timestamp(System.currentTimeMillis());
    }

    @PreUpdate
    void lastModifiedDate() {
        this.lastModifiedDate = new Timestamp(System.currentTimeMillis());
    }


}
