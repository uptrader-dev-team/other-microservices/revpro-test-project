package com.revature.revprotestproject.user;

import com.revature.revprotestproject.Notifications;
import com.revature.revprotestproject.exception.UserNotFoundException;
import com.revature.revprotestproject.response.ResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.InvalidNameException;
import java.util.Optional;

/**
 * Controller used for user persistence
 * Created by: anandak
 * Date created: 8/28/2023
 */

@RestController
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request, missing required fields.", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized.", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden.", response = Error.class),
        @ApiResponse(code = 404, message = "Not Found.", response = Error.class),
        @ApiResponse(code = 406, message = "Not Acceptable.", response = Error.class),
        @ApiResponse(code = 415, message = "Bad Request, missing required fields.", response = Error.class),
        @ApiResponse(code = 429, message = "Bad Request, missing required fields.", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error.", response = Error.class)
})
@Api(tags = "User API")
@RequestMapping(UserController.BASE_URL)
public class UserController {
    public static final String BASE_URL = "api/v1/users";
    public UserService userService;
    public Notifications notifs;
    private final String JSON = MediaType.APPLICATION_JSON_VALUE;

    @Autowired
    public UserController(UserService userService, Notifications notifs) {
        this.userService = userService;
        this.notifs = notifs;
    }

    @ApiOperation(value = "Saves a new user", notes = "Saves a new user to the database")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = User.class) })
    @PostMapping(value = "/new/{name}/{email}", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> persist(
            @PathVariable("name") String userName,
            @PathVariable("email") String userEmail) throws Exception {

        if (userName.matches(".*[(|)]+.*")) {
            throw new InvalidNameException(notifs.user_name_invalid);
        }

        User user = User.builder()
                .userName(userName.trim())
                .userEmail(userEmail.trim())
                .build();

        user = userService.save(user);

        return ResponseBuilder.buildResult(user, HttpStatus.OK);
    }

    @ApiOperation(value = "Returns the details of a given user", notes = "Returns a details of a user by user name")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Long.class) })
    @GetMapping(value = "/get/{name}", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> getUserId (@PathVariable("name") String userName) throws UserNotFoundException {
        Optional<Long> userId = userService.getId(userName);

        if (userId.isEmpty()) {
            throw new UserNotFoundException(notifs.user_not_found);
        }

        return ResponseBuilder.buildResult(userId, HttpStatus.OK);
    }

}
