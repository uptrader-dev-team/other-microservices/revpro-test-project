package com.revature.revprotestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Entry class of application
 * Created by: anandak
 * Date created: 8/28/2023
 */

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EnableConfigurationProperties({Notifications.class})
public class RevProTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(RevProTestProjectApplication.class, args);
	}

}
