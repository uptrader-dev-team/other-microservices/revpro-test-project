package com.revature.revprotestproject.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * JPA entity for persisting message info
 * Created by: anandak
 * Date created: 8/28/2023
 */

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    Long messageId;

    @Column(name = "message")
    String message;

    @Column(name = "user_id")
    Long userId;

    @CreationTimestamp
    @Column(name = "created_date")
    private Timestamp createdDate;

    @Version
    @UpdateTimestamp
    @Column(name = "modified_date")
    private Timestamp lastModifiedDate;

    @PrePersist
    void createdDate() {
        this.createdDate = new Timestamp(System.currentTimeMillis());
    }

    @PreUpdate
    void lastModifiedDate() {
        this.lastModifiedDate = new Timestamp(System.currentTimeMillis());
    }
}
