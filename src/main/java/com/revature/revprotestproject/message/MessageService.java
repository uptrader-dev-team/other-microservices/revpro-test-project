package com.revature.revprotestproject.message;

import com.revature.revprotestproject.Notifications;
import com.revature.revprotestproject.exception.UserNotFoundException;
import com.revature.revprotestproject.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Service layer for exposing message info
 * Created by: anandak
 * Date created: 8/28/2023
 */

@Slf4j
@Service
public class MessageService {
    UserService userService;
    Notifications notifs;
    MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository, UserService userService, Notifications notifs) {
        this.userService = userService;
        this.notifs = notifs;
        this.messageRepository = messageRepository;
    }

    public void editMessage(Message message, Long messageId) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        this.messageRepository.edit(message.message, now, messageId);
    }

    public void deleteMessage(String userName, Optional<Long> messageId) throws UserNotFoundException {
        Optional<Long> userId = userService.getId(userName);

        if (userId.isEmpty()) {
            throw new UserNotFoundException(notifs.user_not_found);
        }

        if (messageId.isEmpty()) {
            messageRepository.delete(userId.get());
        } else {
            messageRepository.deleteById(userId.get(), messageId.get());
        }
    }

    public List<Message> displayAll() { return messageRepository.findAll();
    }

    public List<Message> getMessages(String userName, Optional<Long> messageId) throws UserNotFoundException {
        Optional<Long> userId = userService.getId(userName);

        if (userId.isEmpty()) {
            throw new UserNotFoundException(notifs.user_not_found);
        }

        if (messageId.isEmpty()) {
            return messageRepository.get(userId.get());
        } else {
            return messageRepository.getById(userId.get(), messageId.get());
        }
    }

    public Message saveMessage(@NonNull Message message) {
        return messageRepository.save(message);
    }
}
