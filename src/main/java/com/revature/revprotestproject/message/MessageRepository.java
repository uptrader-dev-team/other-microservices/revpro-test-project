package com.revature.revprotestproject.message;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Long> {

    @Modifying
    @Transactional
    @Query("UPDATE Message m SET m.message = ?1, m.lastModifiedDate = ?2 WHERE m.messageId = ?3")
    void edit(String message, Timestamp ts, Long messageId);

    @Modifying
    @Transactional
    @Query("DELETE FROM Message m WHERE m.userId = ?1")
    void delete(@NonNull Long userId);

    @Modifying
    @Transactional
    @Query("DELETE FROM Message m WHERE m.userId = ?1 AND m.messageId = ?2")
    void deleteById(@NonNull Long userId, Long messageId);

    @NonNull
    Message save(@NonNull Message message);

    @Override
    List<Message> findAll();

    @Query("SELECT m FROM Message m WHERE m.userId = (:id)")
    List<Message> get(@NonNull Long id);

    @Query("SELECT m FROM Message m WHERE m.userId = ?1 AND m.messageId = ?2")
    List<Message> getById(@NonNull Long id, Long messageId);
}
