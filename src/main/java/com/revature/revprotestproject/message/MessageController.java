package com.revature.revprotestproject.message;

import com.revature.revprotestproject.Notifications;
import com.revature.revprotestproject.exception.InvalidMessageException;
import com.revature.revprotestproject.exception.UserNotFoundException;
import com.revature.revprotestproject.response.ResponseBuilder;
import com.revature.revprotestproject.user.UserService;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller used for message persistence
 * Created by: anandak
 * Date created: 8/28/2023
 */

@RestController
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request, missing required fields.", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized.", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden.", response = Error.class),
        @ApiResponse(code = 404, message = "Not Found.", response = Error.class),
        @ApiResponse(code = 406, message = "Not Acceptable.", response = Error.class),
        @ApiResponse(code = 415, message = "Bad Request, missing required fields.", response = Error.class),
        @ApiResponse(code = 429, message = "Bad Request, missing required fields.", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error.", response = Error.class)
})
@Api(tags = "Message API")
@RequestMapping(MessageController.BASE_URL)
public class MessageController {
    private final String JSON = MediaType.APPLICATION_JSON_VALUE;
    public Notifications notifs;
    public MessageService messageService;
    public UserService userService;
    public static final String BASE_URL = "api/v1/messages";

    @Autowired
    public MessageController(MessageService messageService, Notifications notifs, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @ApiOperation(value = "Saves a new message", notes = "Saves a new message to the database")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Message.class) })
    @PostMapping(value = "/new/{user}/{message}", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> persist (
            @PathVariable("user") String userName,
            @PathVariable("message") String userMessage) throws Exception {

        if (userMessage.matches(".*[(|)]+.*")) {
            throw new InvalidMessageException(notifs.message_invalid);
        }

        Optional<Long> ownerId = userService.getId(userName);

        if (ownerId.isEmpty()) {
            throw new UserNotFoundException(notifs.user_not_found);
        }

        Message message = Message.builder().build();
        message.setMessage(userMessage);
        message.setUserId(ownerId.get());

        message = messageService.saveMessage(message);

        return ResponseBuilder.buildResult(message, HttpStatus.OK);
    }

    @ApiOperation(value = "Returns the specified message of a given user", notes = "Returns all or a single message(s) of a given user")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Message.class, responseContainer = "List") })
    @GetMapping(value = "/get/{user}", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> getMessages (
            @PathVariable("user") String userName,
            @RequestParam(value = "id", required = false) Long messageId) throws Exception {

        List<Message> messages = messageService.getMessages(userName, Optional.ofNullable(messageId));

        return ResponseBuilder.buildResult(messages, HttpStatus.OK);
    }

    @ApiOperation(value = "Returns all messages", notes = "This endpoint returns the messages of the user and other users")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Message.class, responseContainer = "List") })
    @GetMapping(value = "/list", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> displayAll() {

        List<Message> messages = messageService.displayAll();

        return ResponseBuilder.buildResult(messages, HttpStatus.OK);
    }

    @ApiOperation(value = "Deletes a given message", notes = "Deletes a given message by user name & message id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    @DeleteMapping(value = "/delete/{user}", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> delete(
            @PathVariable("user") String userName,
            @RequestParam(value = "id", required = false) Long messageId) throws Exception {

        messageService.deleteMessage(userName, Optional.ofNullable(messageId));

        return ResponseBuilder.buildResult(null, HttpStatus.OK);
    }

    @ApiOperation(value = "Edits a message", notes = "Updates a message of a given user")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    @PutMapping(value = "/edit/{user}/{message}/{id}", produces = { JSON })
    @ResponseBody
    ResponseEntity<Object> editMessage(
            @PathVariable("user") String userName,
            @PathVariable("message") String newMessage,
            @PathVariable("id") Long messageId) throws Exception {

        if (newMessage.matches(".*[(|)]+.*")) {
            throw new InvalidMessageException(notifs.message_invalid);
        }

        Optional<Long> ownerId = userService.getId(userName);

        if (ownerId.isEmpty()) {
            throw new UserNotFoundException(notifs.user_not_found);
        }

        Message message = Message.builder()
                .message(newMessage.trim())
                .userId(ownerId.get())
                .messageId(messageId)
                .build();

        messageService.editMessage(message, messageId);

        return ResponseBuilder.buildResult(null, HttpStatus.OK);
    }
}
