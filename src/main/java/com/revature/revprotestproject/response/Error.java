package com.revature.revprotestproject.response;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Class for building error responses
 * Created by: anandak
 * Date created: 8/29/2023
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Error {

    @NotBlank
    Integer errorCode;

    @Size(max = 32768)
    @Pattern(regexp = "^.{0,32768}$")
    String message;

    @Size(max = 100, message = "reference length is limited to 100 characters")
    @Pattern(regexp = "^.{0,100}$")
    String reference;

    @Singular
    @Size(max = 8192)
    List<@NotNull DataError> dataErrors;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class DataError {

        @Size(max = 1024, message = "errorField length is limited to 1024 characters")
        @Pattern(regexp = "^.{0,1024}$")
        String errorField;

        @NotBlank
        @Size(max = 1024, message = "message length is limited to 1024 characters")
        @Pattern(regexp = "^.{0,1024}$")
        String errorMessage;
    }
}
