package com.revature.revprotestproject.response;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

/**
 * Class for building and maintaining uniform API responses
 * Created by: anandak
 * Date created: 8/29/2023
 */

@Slf4j
@Component
public class ResponseBuilder extends ResponseEntity {
    private static final String ERR_MSG_INVALID_VALUE = "Invalid Value";
    private static final String ERR_MSG_UNRECOGNIZED_FIELD = "Unrecognized field";
    private static final String ERR_MSG_JSON_PARSE = "JSON parse error";
    private static final String ERR_MSG_VALIDATION_FAILURE = "Validation failure";

    @Getter
    final ValidatorFactory validatorFactory;

    @Autowired
    public ResponseBuilder(ValidatorFactory validatorFactory) {
        super(HttpStatus.OK);
        this.validatorFactory = validatorFactory;
    }

    @SneakyThrows
    public static ResponseEntity<Object> buildResult(Object input, HttpStatus status) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonResponse = mapper.writeValueAsString(input);
        return new ResponseEntity<>(jsonResponse, status);
    }

    public final ResponseEntity buildErrorResponse(Exception exception) {
        log.error(exception.getMessage());
        if (exception instanceof BindingResult) {
            return handleBindingResult((BindingResult) exception);
        } else if (exception instanceof MethodArgumentNotValidException){
            return handleBindingResult(((MethodArgumentNotValidException) exception).getBindingResult());
        } else if (exception instanceof MissingServletRequestParameterException) {
            MissingServletRequestParameterException ex = (MissingServletRequestParameterException) exception;
            return handleValidationFailure(ex.getParameterName(), ex.getMessage());
        } else if (exception instanceof MethodArgumentTypeMismatchException) {
            MethodArgumentTypeMismatchException ex = (MethodArgumentTypeMismatchException) exception;
            return handleValidationFailure(ex.getName(), ERR_MSG_INVALID_VALUE);
        } else if (exception instanceof HttpMessageConversionException) {
            return handleHttpMessageConversionException((HttpMessageConversionException) exception);
        } else if (exception instanceof ResponseStatusException) {
            return handleResponseStatusException(exception, (ResponseStatusException) exception);
        }
        return handleGeneralError(exception);
    }

    private ResponseEntity handleGeneralError(Exception exception) {
        Error error = Error.builder()
                .errorCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message("Please contact support team to resolve the error. Error: " + exception.getCause().getMessage())
                .reference("Error time : "+ LocalDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(error);
    }

    private ResponseEntity handleBindingResult(BindingResult bindingResult) {
        Error error = Error.builder()
                .errorCode(HttpStatus.BAD_REQUEST.value())
                .message(ERR_MSG_VALIDATION_FAILURE)
                .reference("Error time : "+ LocalDateTime.now())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(error);
    }

    private ResponseEntity handleValidationFailure(String parameterName, String errorMsg) {
        Error.DataError dataError = Error.DataError.builder().build();
        dataError.setErrorField(parameterName);
        dataError.setErrorMessage(errorMsg);

        Error error = Error.builder()
                .errorCode(HttpStatus.BAD_REQUEST.value())
                .message(ERR_MSG_VALIDATION_FAILURE)
                .reference("Error time : "+ LocalDateTime.now())
                .dataError(dataError)
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(error);
    }

    private ResponseEntity handleHttpMessageConversionException(HttpMessageConversionException exception) {
        Error.DataError dataError = Error.DataError.builder().build();
        if(exception.getRootCause() instanceof JsonMappingException) {
            String fieldName = ((JsonMappingException) exception.getRootCause()).getPath().stream()
                    .filter(obj -> obj.getFieldName() != null)
                    .map(JsonMappingException.Reference::getFieldName)
                    .collect(Collectors.joining("."));
            String errorMessage = exception.getMessage()!=null && exception.getMessage().contains(ERR_MSG_UNRECOGNIZED_FIELD) ? ERR_MSG_UNRECOGNIZED_FIELD : ERR_MSG_INVALID_VALUE;
            dataError.setErrorField(fieldName);
            dataError.setErrorMessage(errorMessage);
        }
        Error error = Error.builder()
                .errorCode(HttpStatus.BAD_REQUEST.value())
                .message(ERR_MSG_JSON_PARSE)
                .reference("Error time : "+ LocalDateTime.now())
                .dataError(dataError)
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(error);
    }

    ResponseEntity handleResponseStatusException(Exception exception, ResponseStatusException responseStatusEx) {
        Error error = Error.builder()
                .errorCode(responseStatusEx.getStatus().value())
                .message(exception.getMessage())
                .reference("Error time : "+ LocalDateTime.now())
                .build();

        return ResponseEntity.status(responseStatusEx.getStatus())
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .headers( responseStatusEx.getResponseHeaders())
                .body(error);
    }
}
